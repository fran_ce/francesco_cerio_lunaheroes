# Lunaheroes

Progetto di tirocinio per LunaPartner sviluppato in Angular 10.
Il progetto è composto da due parti:
 - Login
 - Lista di Supereroi

## Run

Per avviare il programma, lanciare il comando `ng serve -o` dalla directory del progetto. 
Si aprirà una finestra di login dove inserire i dati dell'utente e successivamente si aprirà una pagina contente la tabella con la lista degli eroi.

## Scelte implementative

Le due schermate di Login e Lista di Supereroi sono due component differenti e ho implementato un modulo Routing che gestisce la navigazione tra i due. 

Per la tabella ho utilizzato Ag-Grid e con l'ausilio di due service ho gestito la comunicazione di dati tra essa e i dialog per la modifica, eliminazione e visione dei dettagli di ogni singola riga.

Francesco Cerio
