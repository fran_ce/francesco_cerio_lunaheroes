import { AgGridService } from './../ag-grid.service';
import { GridOptions } from 'ag-grid-community';
import { Hero, HeroService } from './../hero.service';
import { AddEditDeleteActionsComponent } from './../add-edit-delete-actions/add-edit-delete-actions.component';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogOverviewComponent } from '../dialog-overview/dialog-overview.component';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})

export class HeroesComponent {
  gridApi;
  private gridColumnApi;
  rowSelection;

  gridOptions: GridOptions;

  constructor(private dialog: MatDialog, private heroes: HeroService, private gridService: AgGridService) {
    this.gridOptions = {
      rowData: this.rowData,
      columnDefs: this.columnDefs,
      rowSelection: 'single',
      onGridReady: (event) => {
          this.gridApi = event.api;
          this.gridColumnApi = event.columnApi;
          this.gridApi.sizeColumnsToFit();
      }
    }

    this.gridService.setGridOptions(this.gridOptions);

  }

  columnDefs = [
        { field: 'Nome', sortable: true, filter: true },
        { field: 'Età', sortable: true, },
        { field: 'Sesso', sortable: true },
        { field: 'SegniParticolari'},
        { field: 'Note'},
        { field: 'Azioni', cellRendererFramework: AddEditDeleteActionsComponent, resizable: true,
        cellRendererParams(params) {
          return {
              test: params.data,
              context: self,
          };
      },
    }
    ];

    rowData = this.heroes.heroesList.map(function(item){
      return {Id: item.id,
              Nome: item.values.nome,
              Età: item.values.eta,
              Sesso: item.values.sesso,
              SegniParticolari: item.values.segni,
              Note: item.values.note};
    });

    autoSizeAll() {
      var allColumnIds = [];
      this.gridColumnApi.getAllColumns().forEach(function (column) {
        allColumnIds.push(column.colId);
      });
      this.gridColumnApi.autoSizeAllColumns(allColumnIds, true);
    }

    openDialog(){
      this.dialog.open(DialogOverviewComponent);
    }
    removeRow(){
      var selRow = this.gridApi.getSelectedRows();
      var i = this.gridOptions.api.applyTransaction({ remove: selRow });
      this.gridOptions.api.refreshCells();

    }

    onSelectionChanged(evt) {
      var selRow = this.gridApi.getSelectedRows();
      console.log(selRow);
      console.log("Hero list ");
      console.log(this.heroes.heroesList);
      console.log("Row Data ");
      console.log(this.gridOptions.rowData);
      this.gridService.setGridOptions(this.gridOptions);

      //this.heroes.setCurrentIdAndRow(selRow[0].Id, selRow);
    }

    onSortChanged(params){
      params.api.forEachNode((rowNode, index) => {
        rowNode.rowIndex = index;
      });
    }


}
