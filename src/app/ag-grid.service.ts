import { Hero, HeroService } from './hero.service';
import { GridOptions, RowNode } from 'ag-grid-community';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AgGridService {
  grid: GridOptions;

  constructor(private heroService: HeroService) { }

  setGridOptions(g: GridOptions){
    this.grid = g;
  }

  deleteRow(){
    var selRow = this.grid.api.getSelectedRows();
    this.grid.rowData.splice(this.heroService.rowId, 1);
    var i = this.grid.api.applyTransaction({ remove: selRow });
  }

  updateRow(hero: Hero, rowId: number){
    var itemsToUpdate = [];
    this.grid.api.forEachNodeAfterFilterAndSort(function(rowNode, index){
      if (!rowNode.isSelected || rowId !== index) {
        return;
      }
      var data = rowNode.data;
      data.Nome = hero.values.nome;
      data.Id = hero.id;
      data.Età = hero.values.eta;
      data.Sesso = hero.values.sesso;
      data.SegniParticolari = hero.values.segni;
      data.Note = hero.values.note;
      itemsToUpdate.push(data);
    });
    var res = this.grid.api.applyTransaction({ update: itemsToUpdate });
  }

  addHeroRow(hero: Hero){
    this.grid.rowData.push({Id: hero.id,
      Nome: hero.values.nome,
      Età: hero.values.eta,
      Sesso: hero.values.sesso,
      SegniParticolari: hero.values.segni,
      Note: hero.values.note
    });
    this.grid.api.setRowData(this.grid.rowData);
    //this.grid.api.applyTransaction({update: this.heroService.heroesList});

    //this.grid.api.refreshCells();
  }


}
