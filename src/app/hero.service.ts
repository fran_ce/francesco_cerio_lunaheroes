import { HeroesComponent } from './heroes/heroes.component';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export interface Hero{
  id: string,
  values: {
    nome: string,
    eta: number,
    sesso: string,
    segni?: string,
    note?: string
  }
}

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  heroesList: Hero[] = [];

  selectedId: string;
  rowId: any;

  constructor() {
    this.heroesList.push({
      id: Math.random().toString(18).substr(2, 17),
      values:
      {nome: 'Batman',
      eta: 30,
      sesso: 'M',
      segni: 'Uomo Pipistrello',
      note: 'Il Cavaliere Oscuro agisce di notte per combattere il crimine. Mentre la città dorme, nella penombra lui agisce per proteggere i cittadini di Gotham City.'}
    })

    this.heroesList.push(
    {
      id: Math.random().toString(18).substr(2, 17),
      values:
      {nome: 'Thor',
      eta: 32,
      sesso: 'M',
      segni: 'Dio del tuono',
      note: 'Il suo martello è invincibile'}
    })
   }

  addHero(obj: Hero){
    this.heroesList.push(obj);
  }

  removeHero(id: string){
    const i = this.myIndexOf(id);
    this.heroesList.splice(i, 1);
  }

  myIndexOf(obj: string){
    for (var i = 0; i < this.heroesList.length; i++) {
      console.log(i);
      if (this.heroesList[i].id === obj) {
          console.log(i);
          return i;
        }
    }
    return -1;
  }

  getCurrId(): string{
    return this.selectedId;
  }

  setCurrentIdAndRow(obj: string, rowId: any){
    this.selectedId = obj;
    this.rowId = rowId;
  }
}
