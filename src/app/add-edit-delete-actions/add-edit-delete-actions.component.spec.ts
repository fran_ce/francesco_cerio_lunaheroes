import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditDeleteActionsComponent } from './add-edit-delete-actions.component';

describe('AddEditDeleteActionsComponent', () => {
  let component: AddEditDeleteActionsComponent;
  let fixture: ComponentFixture<AddEditDeleteActionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditDeleteActionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditDeleteActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
