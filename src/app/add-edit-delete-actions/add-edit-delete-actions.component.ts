import { HeroesComponent } from './../heroes/heroes.component';
import { ViewDialogComponent } from './../view-dialog/view-dialog.component';
import { EditDialogComponent } from './../edit-dialog/edit-dialog.component';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-add-edit-delete-actions',
  templateUrl: './add-edit-delete-actions.component.html',
  styleUrls: ['./add-edit-delete-actions.component.css']
})
export class AddEditDeleteActionsComponent {
  public params: any;

    constructor(private heroList: HeroService, private dialog: MatDialog) {
    }

    agInit(params): void {
        this.params = params;
    }

    public onView<T>(row: T) {
      this.heroList.setCurrentIdAndRow(this.params.data.Id, this.params.rowIndex);
      this.dialog.open(ViewDialogComponent);
    }

    public onEdit<T>(row: T) {
      this.heroList.setCurrentIdAndRow(this.params.data.Id, this.params.rowIndex);
      this.dialog.open(EditDialogComponent);
    }

    public onDelete<T>(row: T) {
      this.heroList.setCurrentIdAndRow(this.params.data.Id, this.params.rowIndex);
      this.dialog.open(DeleteDialogComponent);
    }


}
