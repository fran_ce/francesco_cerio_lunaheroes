import { AgGridService } from './../ag-grid.service';
import { HeroService } from './../hero.service';
import { HeroesComponent } from './../heroes/heroes.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent implements OnInit {
  constructor(private heroList: HeroService, private dialog: MatDialog, private agGridService: AgGridService) {}
  heroName: string = this.heroList.heroesList[this.heroList.myIndexOf(this.heroList.getCurrId())].values.nome;
  ngOnInit(): void {}

  deleteHero(){
    console.log(this.heroList.selectedId);

    this.heroList.removeHero(this.heroList.selectedId);
    this.agGridService.deleteRow();
    this.dialog.closeAll();

  }

  close(){
    this.dialog.closeAll();
  }

}
