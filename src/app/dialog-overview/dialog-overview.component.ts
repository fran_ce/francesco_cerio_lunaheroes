import { AgGridService } from './../ag-grid.service';
import { Hero, HeroService } from './../hero.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-overview',
  templateUrl: './dialog-overview.component.html',
  styleUrls: ['./dialog-overview.component.css']
})
export class DialogOverviewComponent implements OnInit {
  addHeroForm: FormGroup;
  hero: Hero = this.heroService[this.heroService.myIndexOf(this.heroService.getCurrId())];

  constructor(private gridService: AgGridService,
              private heroService: HeroService,
              private formBuilder: FormBuilder,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.addHeroForm = this.formBuilder.group({
      heroName: new FormControl(),
      heroEta: new FormControl(),
      heroSex: new FormControl(),
      heroSegni: new FormControl(),
      heroNote: new FormControl()
    });
  }

  discardChanges(){
    this.dialog.closeAll();
  }

  saveChanges(){
    var newHero: Hero;

    let newId = Math.random().toString(18).substr(2, 17);
    let newName = this.addHeroForm.value.heroName;
    let newEta = this.addHeroForm.value.heroEta;
    let newSex = this.addHeroForm.value.heroSex;
    let newSegni = this.addHeroForm.value.heroSegni;
    let newNote = this.addHeroForm.value.heroNote;

    newHero = {
      id: newId,
      values: {
        nome: newName,
        eta: newEta,
        sesso: newSex,
        segni: newSegni,
        note: newNote
      }
    }

    this.heroService.addHero(newHero);
    this.gridService.addHeroRow(newHero);
    this.dialog.closeAll();
  }

}
