import { AgGridService } from './../ag-grid.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Hero, HeroService } from '../hero.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.css']
})
export class EditDialogComponent implements OnInit {
  editForm: FormGroup;

  heroToEdit: Hero = this.heroes.heroesList[this.heroes.myIndexOf(this.heroes.getCurrId())];
  heroName = this.heroToEdit.values.nome;
  heroEta = this.heroToEdit.values.eta;
  heroSex = this.heroToEdit.values.sesso;
  heroSegni = this.heroToEdit.values.segni;
  heroNote = this.heroToEdit.values.note;

  constructor(private formBuilder: FormBuilder,
              private heroes: HeroService,
              private dialog: MatDialog,
              private agService: AgGridService) { }

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      heroName: [this.heroName, Validators.required],
      heroEta: [this.heroEta, Validators.required],
      heroSex: [this.heroSex , Validators.required],
      heroSegni: this.heroSegni,
      heroNote: this.heroNote
    });
  }

  deleteChanges(){
    this.dialog.closeAll();
  }

  saveChanges(){
    let newName = this.editForm.value.heroName;
    let newEta = this.editForm.value.heroEta;
    let newSex = this.editForm.value.heroSex;
    let newSegni = this.editForm.value.heroSegni;
    let newNote = this.editForm.value.heroNote;

    if (newName == null || newEta == null || newSex == null){
      return;
    }

    this.heroes.heroesList[this.heroes.myIndexOf(this.heroes.getCurrId())].values.nome = newName;
    this.heroes.heroesList[this.heroes.myIndexOf(this.heroes.getCurrId())].values.eta = newEta;
    this.heroes.heroesList[this.heroes.myIndexOf(this.heroes.getCurrId())].values.sesso = newSex;
    this.heroes.heroesList[this.heroes.myIndexOf(this.heroes.getCurrId())].values.segni = newSegni;
    this.heroes.heroesList[this.heroes.myIndexOf(this.heroes.getCurrId())].values.note = newNote;

    this.agService.updateRow(this.heroes.heroesList[this.heroes.myIndexOf(this.heroes.getCurrId())], this.heroes.rowId);
    this.dialog.closeAll();
  }

}
