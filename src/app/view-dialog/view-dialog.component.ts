import { Hero, HeroService } from './../hero.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-view-dialog',
  templateUrl: './view-dialog.component.html',
  styleUrls: ['./view-dialog.component.css']
})
export class ViewDialogComponent implements OnInit {

  hero: Hero = this.heroService.heroesList[this.heroService.myIndexOf(this.heroService.getCurrId())];

  constructor(private dialog: MatDialog, private heroService: HeroService) {
    this.sexToString();
   }

  heroName = this.hero.values.nome;
  heroSegni = this.hero.values.segni;
  heroEta = this.hero.values.eta;
  heroNote = this.hero.values.note;
  heroSex;

  ngOnInit(): void {

  }

  sexToString(){
    const s = this.hero.values.sesso;
    switch (s) {
      case 'M': {
        this.heroSex = 'Uomo';
        break;
      }

      case 'F': {
        this.heroSex = 'Donna';
        break;
      }

      case 'Unicorno': {
        this.heroSex = 'Unicorno';
        break;
      }
    }

  }

  closeDialog(){
    this.dialog.closeAll();
  }

}
